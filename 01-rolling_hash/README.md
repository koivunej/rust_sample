# Simple rolling hash merge instruction output thing

Rolling hash is
[cyclic](https://en.wikipedia.org/wiki/Rolling_hash#Cyclic_polynomial) with
differences (more below). Constant 16 bits of hash value is used which is mixed
in 32 bits of rolling hash.

Compared to rdiff/rsync or their historical descriptions blake2b is used
instead of md5 for rechecking rolling hash matches.

Per spec "apply" operation has not been implemented.

## Rolling hash details

Reimplemented from
[rollinghashcpp](https://github.com/lemire/rollinghashcpp/blob/master/cyclichash.h).
Differs by underlying hash algoritm; not mersenne twister but seahash. This
difference wins only in memory use, loses on bit distribution and is less
performant. I did not want to audit or convert the used mersenne twister
implementation, or look for similar yet stable seedable rng implementation in
rust.

## Compiling etc.

Latest nightly or stable rust should be fine. Implemented with 1.37, tested
with 1.38 and nightly of 2019-10-02.

Compile with `cargo build` or `cargo build --release`.

Run tests with `cargo test`.

Get merge list between two files with `cargo run -- OLD NEW` where `OLD` is the
old file version and `NEW` is the later version.

## Code coverage

Tested with [cov](https://github.com/kennytm/cov):

 1. install with `cargo install cargo-cov`
 2. clean up old results with `cargo cov clean`
 3. execute tests with coverage `cargo cov test`
 4. open results in browser with `cargo cov report --open`
