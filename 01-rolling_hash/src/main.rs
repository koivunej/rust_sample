use std::fs::File;
use std::io;
use std::path::PathBuf;

use structopt::StructOpt;

use rolling_hash_demo::{DeltaCalculator, FileDescriber, MergeOp};

#[derive(Debug, StructOpt)]
struct Opts {
    #[structopt(long = "chunk-len", default_value = "1024")]
    chunk_len: usize,

    #[structopt(parse(from_os_str))]
    /// The older version of a file
    old: PathBuf,

    #[structopt(parse(from_os_str))]
    /// The later version of the same file as <old>
    new: PathBuf,
}

fn main() {
    use std::collections::{BinaryHeap, HashMap};
    use std::env::args;
    use std::process::exit;

    let myname = args()
        .next()
        .unwrap_or_else(|| String::from("rolling_hash_demo"));

    let opts = Opts::from_args();

    let old = match File::open(opts.old) {
        Ok(f) => f,
        Err(e) => {
            eprintln!("{}: Failed to open old file: {}", myname, e);
            exit(1);
        }
    };

    let new = match File::open(opts.new) {
        Ok(f) => f,
        Err(e) => {
            eprintln!("{}: Failed to open new file: {}", myname, e);
            exit(1);
        }
    };

    let (total_old, sig) = match process_file(old, FileDescriber::new(opts.chunk_len)) {
        Ok(t) => t,
        Err(e) => {
            eprintln!("{}: Failed to read old file: {}", myname, e);
            exit(1);
        }
    };

    let (total_new, merge_list) = match process_file(new, DeltaCalculator::new(sig)) {
        Ok(t) => t,
        Err(e) => {
            eprintln!("{}: Failed to read new file: {}", myname, e);
            exit(1);
        }
    };

    eprintln!(
        "# Merge list for chunk size = {}, items = {}",
        opts.chunk_len,
        merge_list.0.len()
    );

    let mut chunk_reuse = HashMap::new();
    let mut ops = [0usize; 2];
    let mut new_bytes = 0u64;

    for (range, op) in merge_list.0 {
        println!("{:?}, {:?}", range, op);
        match op {
            MergeOp::ReuseOldChunk(x) => {
                *chunk_reuse.entry(x).or_insert(0usize) += 1;
                ops[0] += 1;
            }
            MergeOp::UseFromNew => {
                ops[1] += 1;
                new_bytes += range.end - range.start;
            }
        }
    }

    // nice stats and all are out of scope of spec

    let mut heap = BinaryHeap::new();

    for (chunk, count) in chunk_reuse {
        if count < 2 {
            continue;
        }
        heap.push(std::cmp::Reverse((count, chunk)));
        if heap.len() > 10 {
            heap.pop();
        }
    }

    if !heap.is_empty() {
        eprintln!("# 10 most popular old chunks");

        for (i, std::cmp::Reverse((count, chunk))) in heap.into_sorted_vec().into_iter().enumerate()
        {
            eprintln!("# {}. #{} ({} uses)", i + 1, chunk, count);
        }

        eprintln!("#");
    }

    eprintln!("# Summary:");
    eprintln!("#   Old size:      {}", total_old);
    eprintln!("#   New size:      {}", total_new);
    eprintln!("#   Reused chunks: {}", ops[0]);
    eprintln!("#   New parts:     {}", ops[1]);
    eprintln!(
        "#   New bytes:     {} ({:.2} %)",
        new_bytes,
        100.0 * new_bytes as f64 / total_new as f64
    );
}

/// Convinience trait for `FileDescriber` and `DeltaCalculator` to reduce copypaste as they ended
/// up with similar API. Not really important but maybe nicer.
trait FileOperation {
    type Output;

    fn update(&mut self, bytes: &[u8]);

    fn complete(self) -> Self::Output;
}

impl FileOperation for FileDescriber {
    type Output = rolling_hash_demo::FileDescription;

    fn update(&mut self, bytes: &[u8]) {
        FileDescriber::update(self, bytes);
    }

    fn complete(self) -> Self::Output {
        FileDescriber::complete(self)
    }
}

impl FileOperation for DeltaCalculator {
    type Output = rolling_hash_demo::MergeList;

    fn update(&mut self, bytes: &[u8]) {
        DeltaCalculator::update(self, bytes);
    }

    fn complete(self) -> Self::Output {
        DeltaCalculator::complete(self)
    }
}

fn process_file<O: FileOperation>(f: File, mut op: O) -> Result<(usize, O::Output), io::Error> {
    use std::io::{BufRead, BufReader};

    let mut total = 0;

    let mut reader = BufReader::new(f);
    loop {
        let consumed = match reader.fill_buf()? {
            [] => break,
            bytes => {
                op.update(bytes);
                bytes.len()
            }
        };

        total += consumed;
        reader.consume(consumed);
    }
    Ok((total, op.complete()))
}
