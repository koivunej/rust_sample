use super::{RollingHash, RollingHasher};

fn maskfnc(bits: RollingHash) -> RollingHash {
    assert!(bits > 0);
    assert!(bits as usize <= std::mem::size_of::<RollingHash>() * 8);
    let x = 1 << (bits - 1);
    x ^ (x - 1)
}

/// Cyclic or buzhash with different hash function than paper authors version (no MT with large
/// tables but seahash).
#[derive(Debug)]
pub struct CyclicHasher {
    value: RollingHash,
    n: u32,
    l_mask: u32,
    consumed: usize,
}

impl CyclicHasher {
    /// The number of bytes has to be equal to n.
    /// n is the "length of sequences", for trigrams this would be 3.
    /// l is the number of wanted hash bits (0 < l <= 32)
    #[cfg(test)]
    pub fn new(bytes: &[u8], n: u32, l: u32) -> Result<Self, ()> {
        let mut c = CyclicHasher {
            value: 0,
            n,
            consumed: 0,
            l_mask: maskfnc(l),
        };

        for b in bytes {
            c.consume(*b);
        }

        if c.consumed == n as usize {
            Ok(c)
        } else {
            Err(())
        }
    }

    pub fn new_uninitialized(n: u32, l: u32) -> Self {
        CyclicHasher {
            value: 0,
            n,
            consumed: 0,
            l_mask: maskfnc(l),
        }
    }

    pub fn consume(&mut self, b: u8) {
        assert!(
            self.consumed < self.n as usize,
            "consumed({}) >= n({})",
            self.consumed,
            self.n
        );
        self.value = self.value.rotate_left(1) ^ self.hash(b);
        self.consumed += 1;
    }

    pub fn is_initialized(&self) -> bool {
        self.consumed == self.n as usize
    }

    fn hash(&self, b: u8) -> RollingHash {
        use seahash::SeaHasher;
        use std::hash::{Hash, Hasher};

        let mut h = SeaHasher::default();
        b.hash(&mut h);

        // FIXME: this is likely devastating for the hash function itself
        // mersenne twister sounds much better at generating values with an upper bound
        (h.finish() as RollingHash) & self.l_mask
    }

    pub fn reset(&mut self) {
        self.value = 0;
        self.consumed = 0;
    }
}

impl RollingHasher for CyclicHasher {
    fn update(&mut self, removed: u8, added: u8) -> RollingHash {
        assert!(self.is_initialized());
        let new =
            self.value.rotate_left(1) ^ self.hash(removed).rotate_left(self.n) ^ self.hash(added);
        self.value = new;
        self.value
    }

    fn value(&self) -> RollingHash {
        self.value
    }
}

#[cfg(test)]
mod test {

    use super::{CyclicHasher, RollingHasher};

    #[test]
    fn new_failures() {
        CyclicHasher::new(&[0], 2, 16).unwrap_err();
    }

    #[test]
    fn quickcheck_cyclic() {
        use quickcheck::{quickcheck, TestResult};

        fn rolled_is_same_as_only_last_n(bs: Vec<u8>, n: u32, l: u32) -> TestResult {
            if bs.is_empty() {
                return TestResult::discard();
            }

            if n < 1 || n as usize > bs.len() {
                return TestResult::discard();
            }

            if l < 1 || l > 32 {
                return TestResult::discard();
            }

            let through_all = {
                let mut through_all = CyclicHasher::new(&bs[..(n as usize)], n, l).unwrap();
                for i in (n as usize)..bs.len() {
                    through_all.update(bs[i - (n as usize)], bs[i]);
                }
                through_all.value()
            };

            let last_n = CyclicHasher::new(&bs[bs.len() - (n as usize)..], n, l)
                .unwrap()
                .value();

            return TestResult::from_bool(through_all == last_n);
        }

        quickcheck(rolled_is_same_as_only_last_n as fn(Vec<u8>, u32, u32) -> TestResult);
    }
}
