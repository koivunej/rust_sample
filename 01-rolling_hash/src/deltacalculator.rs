use blake2::{Blake2b, Digest};
use std::collections::{HashMap, VecDeque};
use std::ops::Range;

use super::{CyclicHasher, FileDescription, MergeList, MergeOp, RollingHasher};
use super::{RollingHash, StrongHash};

/// Calculates the reusable chunks and a plan (`MergeList`) how to "merge" an old
/// version of a file to a later version.
pub struct DeltaCalculator {
    sig: FileDescription,
    processed_bytes: u64,
    // buffer used for popping last, appending new and also re-checks with strong hasher
    buffer: VecDeque<u8>,
    rolling_hasher: CyclicHasher,
    // multimap from rollinghash to candidate chunks from OLD (identified by stronghash)
    chunks_by_rolling: HashMap<RollingHash, Vec<(usize, StrongHash)>>,

    // collected operations for byte ranges to merge OLD into same NEW
    merge_list: Vec<(Range<u64>, MergeOp)>,
    merged_bytes: u64,
}

#[derive(PartialEq, Eq)]
enum Status {
    InsideFile,
    HitEof,
}

impl DeltaCalculator {
    /// Initializes a new instance for calculating the merge plan for the new version of the file
    /// given the old chunks described in the `FileDescription`.
    pub fn new(sig: FileDescription) -> Self {
        let mut chunks_by_rolling = HashMap::new();

        for (num, (strong, rolling)) in sig.chunks.iter().enumerate() {
            // equal (strong, rolling) tuples are currently "handled"
            // in "match_here"
            chunks_by_rolling
                .entry(*rolling)
                .or_insert_with(Vec::new)
                .push((num, strong.clone()));
        }

        let chunk_len = sig.chunk_len;

        DeltaCalculator {
            sig,
            buffer: VecDeque::with_capacity(chunk_len),
            rolling_hasher: CyclicHasher::new_uninitialized(chunk_len as u32, 16),
            chunks_by_rolling,
            processed_bytes: 0,
            merge_list: Vec::new(),
            merged_bytes: 0,
        }
    }

    /// Called to process all bytes of the input. Always consumes all of the bytes.
    pub fn update(&mut self, mut bytes: &[u8]) {
        while !bytes.is_empty() {
            if self.buffer.len() < self.sig.chunk_len {
                // we need to init the rolling hash

                let next = {
                    let chunk_remaining = self.sig.chunk_len - self.buffer.len();
                    &bytes[0..chunk_remaining.min(bytes.len())]
                };

                bytes = &bytes[next.len()..];

                for b in next {
                    self.rolling_hasher.consume(*b);
                    self.processed_bytes += 1;
                }

                self.buffer.extend(next);

                continue;
            }

            assert!(self.buffer.len() == self.sig.chunk_len);
            assert!(self.rolling_hasher.is_initialized());

            if self.match_here(Status::InsideFile) {
                continue;
            }

            let oldest = self
                .buffer
                .pop_front()
                .expect("Buffer must be full at the moment");
            let latest = bytes[0];

            self.buffer.push_back(latest);
            self.rolling_hasher.update(oldest, latest);
            self.processed_bytes += 1;

            bytes = &bytes[1..];

            assert_eq!(self.buffer.len(), self.sig.chunk_len);
        }
    }

    /// Attempts to match current state as one of the chunks received in the `FileDescription`.
    /// Parameter `completed` changes the operation slightly when the input has been exhausted
    /// (completed == true) and allows for the last chunk to be shorter in length.
    /// Returns `true` on success.
    fn match_here(&mut self, status: Status) -> bool {
        let candidates = match self.chunks_by_rolling.get(&self.rolling_hasher.value()) {
            Some(candidates) => candidates,
            None => return false,
        };

        // need to recheck even just for one candidate
        let (a, b) = self.buffer.as_slices();
        let rechecked = Blake2b::new().chain(a).chain(b).result().to_vec();

        // we might produce 0, 1 or 2 mergeops but we might also find duplicates
        let mut pending = Vec::new();

        for (chunk_num, candidate_hash) in candidates.iter() {
            assert_eq!(candidate_hash.len(), rechecked.len());

            if candidate_hash != &rechecked {
                // worst practice to check hash with plain eq
                // but this app is not sensitive to side channel disclosures
                continue;
            }

            if !pending.is_empty() {
                // even linux kernel tarballs have repeating chunks (license maybe?) so
                // maybe just pick first for now.
                break;
            }

            let range = if self.processed_bytes < self.merged_bytes + self.sig.chunk_len as u64 {
                // short chunk, these should only be found at the end
                assert!(status == Status::HitEof, "short chunk before eof");
                self.merged_bytes..self.processed_bytes
            } else {
                // full chunk
                let end = self.processed_bytes;
                let start = end - self.sig.chunk_len as u64; // FIXME: use everything file related as u64?
                start..end
            };

            if self.merged_bytes < range.start {
                // there exists an unmatched section between where we last ended up
                // and the found chunk
                // this will not happen for "short chunk"
                pending.push((self.merged_bytes..range.start, MergeOp::UseFromNew));
            }

            pending.push((range, MergeOp::ReuseOldChunk(*chunk_num)));
        }

        if !pending.is_empty() {
            self.merge_list.extend(pending);
            self.merged_bytes = self.processed_bytes;
            self.buffer.clear();
            self.rolling_hasher.reset();

            true
        } else {
            false
        }
    }

    /// Consumes `self` and returns the generated merge plan.
    pub fn complete(mut self) -> MergeList {
        if self.merged_bytes < self.processed_bytes && !self.match_here(Status::HitEof) {
            self.merge_list
                .push((self.merged_bytes..self.processed_bytes, MergeOp::UseFromNew))
        }

        MergeList(self.merge_list)
    }
}
