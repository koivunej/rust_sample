use blake2::{Blake2b, Digest};

use super::{CyclicHasher, FileDescription, RollingHasher};
use super::{RollingHash, StrongHash};

/// An i/o object for generating a single FileDescription without deciding which
/// i/o API to use.
pub struct FileDescriber {
    // Stash for unprocessed bytes between full chunks
    chunks: Vec<(StrongHash, RollingHash)>,
    chunk_len: usize,
    len: usize,

    chunk_consumed: usize,
    strong_hasher: Blake2b,
    rolling_hasher: CyclicHasher,
}

impl FileDescriber {
    /// `chunk_len` determines the reusable chunk size when later trying to save bytes by
    /// not copying everything over a possibly slow transport.
    pub fn new(chunk_len: usize) -> Self {
        assert!(chunk_len < std::u32::MAX as usize);
        Self {
            chunks: Vec::new(),
            chunk_len,
            len: 0,

            chunk_consumed: 0,
            strong_hasher: Blake2b::new(),
            rolling_hasher: CyclicHasher::new_uninitialized(chunk_len as u32, 16),
        }
    }

    /// Processes the given input a `chunk_len` at a time but always consumes all of the slice.
    pub fn update(&mut self, mut bytes: &[u8]) {
        while !bytes.is_empty() {
            let next = {
                let chunk_remaining = self.chunk_len - self.chunk_consumed;
                &bytes[0..chunk_remaining.min(bytes.len())]
            };

            bytes = &bytes[next.len()..];

            self.strong_hasher.input(next);
            for b in next {
                self.rolling_hasher.consume(*b);
            }

            self.chunk_consumed += next.len();
            self.len += next.len();

            if self.chunk_len - self.chunk_consumed == 0 {
                self.finish_chunk();
            }
        }
    }

    fn finish_chunk(&mut self) {
        assert!(self.chunk_consumed > 0);
        let strong_hash = self.strong_hasher.result_reset().to_vec();
        let rolling_hash = self.rolling_hasher.value();
        self.rolling_hasher.reset();

        self.chunks.push((strong_hash, rolling_hash));
        self.chunk_consumed = 0;
    }

    /// Once the input has been exhausted with calls to `update`, call this to consume to get
    /// access to the `FileDescription`.
    pub fn complete(mut self) -> FileDescription {
        if self.chunk_consumed > 0 {
            self.finish_chunk();
        }

        FileDescription {
            chunks: self.chunks,
            chunk_len: self.chunk_len,
            len: self.len,
        }
    }
}
