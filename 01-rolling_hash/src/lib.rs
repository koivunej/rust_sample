extern crate blake2;

#[cfg(test)]
extern crate quickcheck;

use std::ops::Range;

mod describer;
pub use describer::FileDescriber;

mod deltacalculator;
pub use deltacalculator::DeltaCalculator;

mod cyclic;
use cyclic::CyclicHasher;

/// For example SHA-256 or blake2b
type StrongHash = Vec<u8>;

/// Weaker but constant time "remove last and add new" updateable
type RollingHash = u32;

/// Description of a file. If the existing chunks are still present in the new version of the
/// same file can be reused and later merged. [`rdiff(1)`] calls this a "signature."
///
/// [`rdiff(1)`]: https://linux.die.net/man/1/rdiff
#[derive(Debug, Clone)]
pub struct FileDescription {
    /// For each chunk in the file a strong and weaker rolling hash is presented
    chunks: Vec<(StrongHash, RollingHash)>,
    /// Size of chunk used
    chunk_len: usize,
    /// Original file length
    len: usize,
}

/// Merge list describes how re-using the previous version of a file it can be made into match
/// the new version. It is calculated by `DeltaCalculator` from `FileDescription` of the old
/// version and the bytes of a new version.
#[derive(Debug, PartialEq, Eq)]
pub struct MergeList(pub Vec<(Range<u64>, MergeOp)>);

/// The different kinds of operations to do during merging.
#[derive(Debug, PartialEq, Eq)]
pub enum MergeOp {
    /// Reuse the given chunk number from the old file
    ReuseOldChunk(usize),
    /// Use this range from the new file ("transferred some other way")
    UseFromNew,
}

/// Rolling hasher trait. Currently only implemented by `CyclicHasher` and not so well as it
/// requires initialization which is probably best left outside of this trait.
trait RollingHasher {
    /// Removes the oldest `removed` and adds the new value `added`
    fn update(&mut self, removed: u8, added: u8) -> RollingHash;

    /// Returns the current value of the hash
    fn value(&self) -> RollingHash;
}
