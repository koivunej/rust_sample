extern crate quickcheck;

use rolling_hash_demo::{DeltaCalculator, FileDescriber, MergeList, MergeOp};

#[test]
fn matching_file() {
    let old = b"01234567891";
    let new = b"01234567891";

    assert_eq!(old.len(), 11);

    let expected = MergeList(vec![
        (0..4, MergeOp::ReuseOldChunk(0)),
        (4..8, MergeOp::ReuseOldChunk(1)),
        (8..11, MergeOp::ReuseOldChunk(2)),
    ]);

    assert_eq!(merge_list_for(old, new), expected);
}

#[test]
fn shuffled_file() {
    let old = b"0123456789ab";
    let new = b"456789ab0123";

    assert_eq!(old.len(), 12);

    let expected = MergeList(vec![
        (0..4, MergeOp::ReuseOldChunk(1)),
        (4..8, MergeOp::ReuseOldChunk(2)),
        (8..12, MergeOp::ReuseOldChunk(0)),
    ]);

    assert_eq!(merge_list_for(old, new), expected);
}

#[test]
fn inserted_at_boundary() {
    let old = b"0123456789ab";
    let new = b"0123 this 456789ab";

    let expected = MergeList(vec![
        (0..4, MergeOp::ReuseOldChunk(0)),
        (4..10, MergeOp::UseFromNew),
        (10..14, MergeOp::ReuseOldChunk(1)),
        (14..18, MergeOp::ReuseOldChunk(2)),
    ]);

    assert_eq!(merge_list_for(old, new), expected);
}

#[test]
fn inserted_middle_of_chunk() {
    let old = b"0123456789ab";
    let new = b"01 this 456789ab";

    let expected = MergeList(vec![
        (0..8, MergeOp::UseFromNew),
        (8..12, MergeOp::ReuseOldChunk(1)),
        (12..16, MergeOp::ReuseOldChunk(2)),
    ]);

    assert_eq!(merge_list_for(old, new), expected);
}

#[test]
fn less_than_chunk_inserted_after_chunk() {
    let old = b"0123456789ab";
    let new = b"0123th456789ab";
    //              ^^ added

    let expected = MergeList(vec![
        (0..4, MergeOp::ReuseOldChunk(0)),
        (4..6, MergeOp::UseFromNew),
        (6..10, MergeOp::ReuseOldChunk(1)),
        (10..14, MergeOp::ReuseOldChunk(2)),
    ]);

    assert_eq!(merge_list_for(old, new), expected);
}

#[test]
fn prepended() {
    let old = b"0123456789ab";
    let new = b"this 0123456789ab";

    let expected = MergeList(vec![
        (0..5, MergeOp::UseFromNew),
        (5..9, MergeOp::ReuseOldChunk(0)),
        (9..13, MergeOp::ReuseOldChunk(1)),
        (13..17, MergeOp::ReuseOldChunk(2)),
    ]);

    assert_eq!(merge_list_for(old, new), expected);
}

#[test]
fn appended() {
    let old = b"0123456789ab";
    let new = b"0123456789ab this";

    let expected = MergeList(vec![
        (0..4, MergeOp::ReuseOldChunk(0)),
        (4..8, MergeOp::ReuseOldChunk(1)),
        (8..12, MergeOp::ReuseOldChunk(2)),
        (12..17, MergeOp::UseFromNew),
    ]);

    assert_eq!(merge_list_for(old, new), expected);
}

#[test]
fn duplicates_in_old() {
    // in manual testing the kernel 5.3.1 and 5.3.2 tarballs have lots of common chunks at
    // chunk_len = 1024
    let old = b"012301234567";
    let new = b"0123456789ab";

    let expected = MergeList(vec![
        (0..4, MergeOp::ReuseOldChunk(0)),
        (4..8, MergeOp::ReuseOldChunk(2)),
        (8..12, MergeOp::UseFromNew),
    ]);

    assert_eq!(merge_list_for(old, new), expected);
}

#[test]
fn shorter_new() {
    let old = b"01234567";
    let new = b"0123";

    let expected = MergeList(vec![(0..4, MergeOp::ReuseOldChunk(0))]);

    assert_eq!(merge_list_for(old, new), expected);
}

fn merge_list_for(old: &[u8], new: &[u8]) -> MergeList {
    let sig = {
        let mut d = FileDescriber::new(4);
        d.update(&old[..]);
        d.complete()
    };

    let mut d = DeltaCalculator::new(sig);
    d.update(&new[..]);
    d.complete()
}
