extern crate chrono;

#[macro_use]
extern crate derive_more;

use chrono::prelude::*;
use std::io::{self, BufRead, BufReader, ErrorKind, Write};
use std::net::{IpAddr, SocketAddr, TcpListener, TcpStream};
use std::sync::mpsc::{channel, Receiver, Sender, TryRecvError};
use std::sync::Arc;
use std::sync::Mutex;
use std::thread::spawn;
use std::time::Duration;
use structopt::StructOpt;

mod proto {
    use chrono::prelude::*;
    use serde::{Deserialize, Serialize};

    #[derive(Debug, Serialize, Deserialize, From)]
    #[serde(tag = "type")]
    pub enum Messages {
        ListenerHello,
        ConnectorHello {
            nick: String,
        },
        ChatMessage2 {
            when: DateTime<Local>,
            msg: String,
        },
        ChatMessage3 {
            when: DateTime<Local>,
            nick: String,
            msg: String,
        },
    }
}

#[derive(Debug, StructOpt)]
struct Opts {
    /// Your nickname
    nick: String,
    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(Debug, StructOpt)]
enum Command {
    /// Connect to another instance of this magnificient chat app
    Connect {
        /// Network address and port of another instance, as reported by another instance
        target: String,
    },
    /// Start listening for others to connect to
    Listen {
        /// Network address to listen or bind on
        bind_ip: IpAddr,
    },
}

fn main() {
    use std::process::exit;
    let opts = Opts::from_args();

    let res = match opts.cmd {
        Command::Connect { target } => run_connector(target, opts.nick),
        Command::Listen { bind_ip } => run_listener(bind_ip, opts.nick),
    };

    if let Err(s) = res {
        eprintln!("{}", s);
        exit(1);
    }
}

fn run_connector(target: String, nick: String) -> Result<(), String> {
    let mut transport = match TcpStream::connect(&target).and_then(Transport::new) {
        Ok(s) => s,
        Err(e) => return Err(format!("Failed to connect to '{}': {}", target, e)),
    };

    match transport.read_one() {
        Ok(proto::Messages::ListenerHello) => {}
        Ok(unexpected) => return Err(format!("Unexpected hello message: {:?}", unexpected)),
        Err(e) => return Err(format!("Read failed: {:?}", e)),
    }

    let hello = proto::Messages::ConnectorHello {
        nick: String::clone(&nick),
    };

    if let Err(e) = transport.write(&hello) {
        return Err(format!("Write failed: {:?}", e));
    }

    let (ui_tx, ui_rx) = channel();
    let (to_client, from_ui) = channel();

    let ui = spawn(move || simple_ui(ui_rx, to_client));

    'main: loop {
        let msg = match transport.read() {
            Ok(Some(proto::Messages::ChatMessage3 { when, nick, msg })) => {
                Some(UiMessage::ChatMessage(when, nick, msg))
            }
            Ok(Some(unexpected)) => Some(UiMessage::DisconnectedOnError(format!(
                "Unexpected message: {:?}",
                unexpected
            ))),
            Ok(None) => None,
            Err(TransportError::Eof) => Some(UiMessage::Disconnected),
            Err(e) => Some(UiMessage::DisconnectedOnError(format!(
                "Read failed: {:?}",
                e
            ))),
        };

        let stop = match &msg {
            Some(UiMessage::Disconnected) | Some(UiMessage::DisconnectedOnError(_)) => true,
            _ => false,
        };

        if let Some(msg) = msg {
            ui_tx.send(msg).unwrap();
        }

        if stop {
            break 'main;
        }

        'messages: loop {
            match from_ui.try_recv() {
                Ok(ClientMessage::ChatMessage(when, msg)) => {
                    let payload = proto::Messages::ChatMessage2 { when, msg };

                    if let Err(e) = transport.write(&payload) {
                        ui_tx
                            .send(UiMessage::DisconnectedOnError(format!(
                                "Write failed: {:?}",
                                e
                            )))
                            .unwrap();
                        break 'main;
                    }
                }
                Ok(ClientMessage::Sender(_)) => unreachable!(),
                Err(TryRecvError::Empty) => {
                    break 'messages;
                }
                Err(TryRecvError::Disconnected) => {
                    break 'main;
                }
            }
        }
    }

    ui.join().unwrap();

    Ok(())
}

fn run_listener(bind_ip: IpAddr, nick: String) -> Result<(), String> {
    let listener = match TcpListener::bind(SocketAddr::new(bind_ip, 0)) {
        Ok(l) => l,
        Err(e) => return Err(format!("Failed to bind to {}:0: {}", bind_ip, e)),
    };

    println!(
        "# Listening on {} for connections",
        listener.local_addr().unwrap()
    );

    let senders: Arc<Mutex<Vec<Sender<ClientMessage>>>> = Arc::new(Mutex::new(Vec::new()));

    let (to_clients, from_ui) = channel();

    let (to_ui, ui_rx) = channel();

    spawn(move || simple_ui(ui_rx, to_clients));

    {
        let senders: Arc<Mutex<Vec<_>>> = Arc::clone(&senders);
        spawn(move || {
            for m in from_ui.iter() {
                let mut g = senders.lock().unwrap_or_else(|p| p.into_inner());
                // use retain to remove senders connected to dropped receivers
                g.retain(move |s| s.send(m.clone()).is_ok());
            }
        });
    }

    for stream in listener.incoming() {
        if let Ok(stream) = stream {
            let server_nick = nick.clone();

            let (to_client, from_server) = channel();

            to_client
                .send(ClientMessage::Sender(to_ui.clone()))
                .unwrap();

            {
                let mut g = senders.lock().unwrap_or_else(|p| p.into_inner());
                g.push(to_client);
            }

            spawn(move || serve_client(stream, from_server, server_nick));
        }
    }

    Ok(())
}

fn serve_client(s: TcpStream, from_server: Receiver<ClientMessage>, server_nick: String) {
    let to_ui = match from_server.recv() {
        Ok(ClientMessage::Sender(tx)) => tx,
        Ok(_) => panic!("Invalid first message from server to serve_client"),
        Err(_) => return,
    };

    let mut transport = Transport::new(s).unwrap();

    let (peer_addr, nick) = match handshake_client(&mut transport) {
        Ok(t) => t,
        Err(e) => {
            let _ = to_ui.send(UiMessage::HandshakeFailure(format!("{:?}", e)));
            return;
        }
    };

    to_ui
        .send(UiMessage::Join(nick.clone(), peer_addr))
        .unwrap();

    let exit_reason;

    'main: loop {
        match transport.read() {
            Ok(Some(proto::Messages::ChatMessage2 { when, msg })) => {
                to_ui
                    .send(UiMessage::ChatMessage(when, nick.clone(), msg))
                    .unwrap();
            }
            Ok(Some(_)) => {
                exit_reason = String::from("Unexpected message");
                break 'main;
            }
            Ok(None) => {}
            Err(e) => {
                exit_reason = format!("Read error: {:?}", e);
                break 'main;
            }
        }

        'messages: loop {
            match from_server.try_recv() {
                Ok(ClientMessage::ChatMessage(when, msg)) => {
                    let msg = proto::Messages::ChatMessage3 {
                        when,
                        nick: server_nick.clone(),
                        msg,
                    };

                    if let Err(e) = transport.write(&msg) {
                        exit_reason = format!("Write error: {:?}", e);
                        break 'main;
                    }
                }
                Ok(_) => {
                    exit_reason = String::from("Unexpected message");
                    break 'main;
                }
                Err(TryRecvError::Empty) => break 'messages,
                Err(TryRecvError::Disconnected) => return,
            }
        }
    }

    let _ = to_ui.send(UiMessage::Leave(nick, peer_addr, exit_reason));
}

#[derive(Debug, From)]
enum HandshakeError {
    Transport(TransportError),
    BadResponse,
}

fn handshake_client(transport: &mut Transport) -> Result<(SocketAddr, String), HandshakeError> {
    let peer_addr = transport.peer_addr()?;

    match transport.exchange(&proto::Messages::ListenerHello)? {
        proto::Messages::ConnectorHello { nick } => Ok((peer_addr, nick)),
        _ => Err(HandshakeError::BadResponse),
    }
}

/// Messages shown to the user
#[derive(Debug)]
enum UiMessage {
    ChatMessage(DateTime<Local>, String, String),
    Join(String, SocketAddr),
    Leave(String, SocketAddr, String),
    HandshakeFailure(String),
    Disconnected,
    DisconnectedOnError(String),
}

impl std::fmt::Display for UiMessage {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            UiMessage::ChatMessage(when, who, what) => write!(fmt, "[{}] {}: {}", when, who, what),
            UiMessage::Join(who, from) => write!(fmt, "** {} joined from {}", who, from),
            UiMessage::Leave(who, from, why) => {
                write!(fmt, "** {} from {} left: {}", who, from, why)
            }
            UiMessage::HandshakeFailure(why) => write!(fmt, "** Hanshake failed: {}", why),
            UiMessage::Disconnected => write!(fmt, "** Disconnected **"),
            UiMessage::DisconnectedOnError(error) => {
                write!(fmt, "** Disconnected on error: {}", error)
            }
        }
    }
}

/// Messages communicated from UI to "client" which is the either the connection to a listener or
/// all currently connected connectors to a listener.
#[derive(Clone)]
enum ClientMessage {
    Sender(Sender<UiMessage>),
    ChatMessage(DateTime<Local>, String),
}

fn simple_ui(ui_rx: Receiver<UiMessage>, to_clients: Sender<ClientMessage>) {
    use std::io::stdin;

    let owned_stdin = stdin();

    let mut locked = owned_stdin.lock();
    let mut buffer = String::new();
    let mut messages = Vec::new();
    let mut connected = true;

    println!("# Exit with CTRL-C");
    println!("# Try /help and /read");

    while connected {
        print!("?");
        if messages.is_empty() {
            print!(" ");
        } else {
            print!("!");
        }
        print!(" ");

        std::io::stdout().flush().unwrap();
        match locked.read_line(&mut buffer) {
            Ok(0) => {
                // eof
                break;
            }
            Ok(_) => {}
            Err(e) => {
                // broken pipe?
                eprintln!("Exiting after {}", e);
                break;
            }
        }

        let drain_after = match buffer.trim() {
            "/help" => {
                println!("HELP:");
                println!("  /read   - print all unprinted messages");
                println!("  /help   - print this help");
                println!("  <empty> - refresh status");
                println!("  otherwise what you write is broadcast to all connected");
                println!("NOTE: nothing happens unless you keep refreshing with empty input");
                false
            }
            "/read" => true,
            "" => false,
            any if any.starts_with('/') => {
                println!("Unsupported command: {}", any);
                false
            }
            other => {
                if let Err(e) = to_clients.send(ClientMessage::ChatMessage(
                    Local::now(),
                    String::from(other),
                )) {
                    println!("Sending failed: {}", e);
                    connected = false;
                }
                false
            }
        };

        'recv_all: loop {
            let next = ui_rx.try_recv();

            match next {
                Ok(x @ UiMessage::Disconnected) | Ok(x @ UiMessage::DisconnectedOnError(_)) => {
                    println!("{:?}", x);
                    connected = false;
                    break 'recv_all;
                }
                Ok(m) => messages.push(m),
                Err(TryRecvError::Empty) => break 'recv_all,
                Err(TryRecvError::Disconnected) => {
                    connected = false;
                    break 'recv_all;
                }
            }
        }

        if drain_after {
            for x in messages.drain(..) {
                println!("{}", x);
            }
        }

        buffer.clear();
    }

    for x in messages.drain(..) {
        println!("{}", x);
    }
}

/// Simple abstraction over blocking `TcpStream` which can en/decode the protocol messages.
/// It does not however handle the protocol, which is implemented in the beginning of serving a
/// client or connecting to a connector.
struct Transport {
    io: BufReader<TcpStream>,
    buffer: String,
}

#[derive(Debug, From)]
enum TransportError {
    Json(serde_json::error::Error),
    Io(io::Error),
    Eof,
}

impl Transport {
    /// Wraps the given `TcpStream` and configures it to have a small read timeout
    fn new(s: TcpStream) -> Result<Self, io::Error> {
        s.set_read_timeout(Some(Duration::from_millis(10)))?;

        Ok(Transport {
            io: BufReader::new(s),
            buffer: String::new(),
        })
    }

    fn peer_addr(&self) -> Result<SocketAddr, TransportError> {
        Ok(self.io.get_ref().peer_addr()?)
    }

    fn write(&mut self, msg: &proto::Messages) -> Result<(), TransportError> {
        serde_json::to_writer(self.io.get_mut(), msg)?;
        self.io.get_mut().write_all(b"\n")?;
        self.io.get_mut().flush()?;
        Ok(())
    }

    /// Returns `Some(proto::Messages)` if there is a whole message in the buffer, `None` if not
    /// enough bytes have been read and errors in case a `TransportError` happens.
    fn read(&mut self) -> Result<Option<proto::Messages>, TransportError> {
        match self.io.read_line(&mut self.buffer) {
            Ok(0) => Err(TransportError::Eof),
            Ok(_) => {
                let msg: proto::Messages = serde_json::from_str(self.buffer.trim())?;
                self.buffer.clear();
                Ok(Some(msg))
            }
            Err(ref e) if e.kind() == ErrorKind::WouldBlock || e.kind() == ErrorKind::TimedOut => {
                Ok(None)
            }
            Err(e) => Err(TransportError::Io(e)),
        }
    }

    /// Blocks until a single message has been read.
    fn read_one(&mut self) -> Result<proto::Messages, TransportError> {
        loop {
            match self.read()? {
                Some(msg) => return Ok(msg),
                None => continue,
            }
        }
    }

    /// Sends and reads a message.
    fn exchange(&mut self, msg: &proto::Messages) -> Result<proto::Messages, TransportError> {
        self.write(msg)?;
        Ok(self.read_one()?)
    }
}
