# P2P Chat example

## Running

There are two modes, listener and connector. Start listener with
`cargo run -- nick1 listener 127.0.0.1` where the last parameter is the bind
address. This will output something like:

```
# Listening on 127.0.0.1:46651 for connections
# Exit with CTRL-C
# Try /help and /read
?
```

Now you can start the listener(s) by `cargo run -- nickN listener 127.0.0.1:46651`.

### UI

UI is pretty minimal. Prompt has two modes:

 * idle: "`?  `"
 * unread messages: "`?! `"

The messages read buffered only once a line has been read, so just press enter
when you think something should have happened.

UI accepts "irc/pop3-alike" commands:

 * `/read` to read new buffered messages
 * `/help` to display a helpful help
 * all other `/` prefixed commands are unsupported
 * empty lines do nothing but refresh the prompt
 * non-empty lines are chat messages sent to all participants

# Structure, quality

This was an attempt in quick-and-dirty rust programming, it did not go so well
as a lot of time was still spent. I had my doubts with the `BufReader` being
able to read the `TcpStream` given read timeouts but it worked well.

I did play around with `termion` but quickly realized that it was out of scope.

# Protocol

"Listener" and "connectors" (or server and clients) send messages as compact
JSON and a newline ("\n"). Upon connection, the listener first sends a
ListenerHello and receives a ConnectorHello with a nick of the new connection.

I haven't actually at no point looked at the json but just used `serde_json`.
The message structure should be something along the lines of:

```
{
  "type": discriminant,
  ...fields
}
```

Where discriminant is the `#[serde(tag = "type")]` it uses to distinguish
different kinds of enum values. Fields are best documented under `proto::Messages`.
